# H2 Database Docker
this help you run latest version  of [H2 Database](http://h2database.com)
Just run the docker build from this project folder to create your docker image.
by default the H2 server console is available on port 81 while the database listner on 1521
You can customize the docker listnening ports just changing in the Dockerfile and then build your image with:
```
docker build  -t sciamlab/h2 .
```

## running the docker
Just launch the following:

```
docker run -d --name camunda_h2 -p 1521:1521 sciamlab/h2
```

## connect to the console

connect to the H2 using your browser on http://<H2_DOCKER_IP>:81/

pay attention the the JDBC URL as by default this is set to store data in the root user home
The correct url is `jdbc:h2:./test` and will store it in the `/opt/h2/data` folder
or if you prefer you can create and connect to your database via TCP using the listner port and a JDBC URL like `jdbc:h2:tcp://<H2_DOCKER_IP>:1521/./test` 




## Connecting from other docker applications
If you want connect to H2 from other dockerized applications you can link the H2 docker to your application using the --link parameter.
For example to run a dockerized [Camunda BPM](https://github.com/camunda/docker-camunda-bpm-platform) and connect to your H2 docker you can use the following:

```
docker run -d --name camunda \
   -p 8080:8080 \ 
   --link camunda_h2 \
   -e DB_DRIVER=org.h2.Driver \
   -e DB_URL=jdbc:h2:tcp://<H2_DOCKER_IP>:1521/./camunda \
   -e DB_USERNAME=sa \
   -e DB_PASSWORD= \
   camunda/camunda-bpm-platform:latest
```


